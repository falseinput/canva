import { Canva } from '../src/canva.js';
import { Obj } from '../src/obj.js'
import { Tree } from './tree.js';

const canva = new Canva({
  element: document.getElementById('canvas')
});

const myObj = new Tree({
  posX: 100,
  posY: 100,
  speed: 10,
  hitAreas: {
    x: [0, 0, 50, 50],
    y: [0, 50, 50, 0]
  },
  move(x, y) {
    // this.posX = this.posX+1;
  }
});

const secondObj = new Tree({
  posX: 10,
  posY: 10,
  hitAreas: {
    x: [0, 0, 50, 50],
    y: [0, 50, 50, 0]
  },
  move(x, y) {
  }
});

const selected = new Map();

const toggleSelect = (obj) => {
  obj.toggleSelect();

  if (selected.delete(obj)) {
    return;
  }

  selected.set(obj, true);
}

canva.add(myObj, secondObj);
secondObj.on('rightClick', toggleSelect.bind(null, secondObj));
myObj.on('rightClick', toggleSelect.bind(null, myObj));

const handleCanvaClick = function(position) {
  let i =0;
  for (const item of selected.keys()) {
    canva.delegate(item, 'moveTo', [position]);
  }
}

canva.addListener('click', handleCanvaClick);

export function runGame() {
  canva.loop(() => {
    canva.objects.forEach((obj) => {
      obj.move();
      obj.draw();
    });
  });
};
