import { Obj } from '../src/obj.js';

export class Tree extends Obj {
  constructor(params) {
    super(params);

    this.speed = 2;

    this.color = [0, 0, 0];
  }

  changeColor() {
    this.color = this.color.map((col => Math.random()*255));
  }

  draw() {
    this.context.save();

    if (this.isSelected) {
      this.context.strokeStyle = 'rgb(255, 0, 0)';
    }

    this.context.fillStyle = `rgb(${this.color[0]}, ${this.color[1]}, ${this.color[2]})`;
    this.context.strokeRect(this.posX, this.posY, 50, 50);

    this.context.restore();
  }
};
