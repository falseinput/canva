import { isObject } from './helpers.js';
import { Obj } from './obj.js';
import { CanvaError } from './exceptions/canva-error.js';


function validateElement(element, message) {
  if (element instanceof Element) {
    return element;
  }

  throw new CanvaError(message);
}

export const canvaFieldsDescriptor = {
  element: {
    required: true,
    validator: validateElement,
    message: 'Canva error: element must be a DOM node. Check your Canva params.'
  }
};

export function validateObject(object, message, fieldsDescriptor = {}) {
  if (isObject(object)) {

    for (const field in fieldsDescriptor) {
      if (!object[field] && canvaFieldsDescriptor[field].required) {
        throw new Error(`Canva error: Param ${field} is required`);

      } else if (object.hasOwnProperty(field)) {
        canvaFieldsDescriptor[field].validator(object[field], canvaFieldsDescriptor[field].message);
      }
    }

    return object;
  }

  throw new CanvaError(message);
}

export function validateObj(object, message) {
  if (object instanceof Obj) {
    return obj;
  }

  throw new CanvaError(message);
}

export function validateEvent(event, eventTypes) {
  if (eventTypes.indexOf(event) > -1) {
    return event;
  }

  throw new CanvaError(`Canva error: unknown event type: ${event}`);
}
