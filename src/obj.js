import { getUniqueId } from './helpers.js';
import { validateEvent } from './validators.js';


const eventTypes = [
  'rightClick'
];

export class Obj {

  constructor(params) {
    /**
     * @type {CanvasRenderingContext2D}
     */
    this.context = null;

    /**
     * @type {Array<Object<type : string, list :Map<Function, Boolean>>}
     */
    this.events = [];
    this.isSelected = false;
    this.speed = 1;

    this.id = getUniqueId();

    Object.assign(this, params);
  }

  getId() {
    return this.id;
  }

  setContext(context) {
    this.context = context;
  }

  draw() {
  }

  on(type, callback) {
    type = validateEvent(type, eventTypes);

    const eventIndex = this.events.findIndex(event => event.type === type);

    if (eventIndex > -1) {
      this.events[eventIndex].list.set(callback, true);
    } else {
      this.events.push({
        type: type,
        list: (new Map()).set(callback, true)
      });
    }
  }


  off(type, callback) {
    type = validateEvent(type, eventTypes);

    const eventIndex = this.events.findIndex(event => event.type === type);

    if (eventIndex > -1) {
      this.events[eventIndex].list.delete(callback);
    }
  }

  toggleSelect() {
    this.isSelected = !this.isSelected;
  }

  moveTo(pos) {
    let a = (pos.y - this.posY) / (pos.x - this.posX);

    let cos =  (pos.x - this.posX) / Math.sqrt(Math.pow((pos.y - this.posY), 2) + Math.pow((pos.x - this.posX), 2), 2)
    let b = pos.y - a * pos.x;

    return () => {
      if (Math.abs(this.posX - pos.x) > 1) {
        if (this.posX > pos.x) {
          this.posX = this.posX - Math.abs(cos) * this.speed;
        } else {
          this.posX = this.posX + Math.abs(cos) * this.speed;
        }
        this.posY = a * (this.posX) + b;

        return false;
      }

      return true;
    }
  }
};
