
/**
 * @description Quite self explaining name, isn't it?
 * @returns {Boolean}
 */
export function isPointInsidePolygon(nvert, vertx, verty, testx, testy) {
  let i, j;
  let c = false;

  for (i = 0, j = nvert-1; i < nvert; j = i++) {
    if ( ((verty[i]>testy) != (verty[j]>testy)) && (testx < (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]))
       c = !c;
  }

  return c;
}
