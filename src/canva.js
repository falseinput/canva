import { Obj } from './obj.js'
import { isPointInsidePolygon } from './geometry.js';
import { validateObject, canvaFieldsDescriptor } from './validators.js';


export class Canva {

  constructor(params) {
    this.element = null;
    this.context = null;
    this.objects = [];
    this.delegated = new Map();


    this.init(params);
    this.bindEvents();
  }

  /**
   * @description Setup new Canva
   * @param {Object} params
   * @throws {TypeError}
   */
  init(params) {
    this.params = validateObject(params, 'Canva error: Canva constructor requires object as argument.', canvaFieldsDescriptor);

    this.element = this.params.element;
    this.context = this.element.getContext('2d');
  }

  /**
   * @description Adds object and sets context
   * @param {Object} params
   * @throws {TypeError}
   */
  add(...objects) {
    objects.forEach(obj => {
      obj = validateObject(obj, 'Canva error: Argument must be instance of Obj. Check add method.');

      obj.setContext(this.context);
      this.objects.push(obj);

    });
  }

  bindEvents() {
    this.element.addEventListener('click', this.handleObjectEvent.bind(this, 'click'));
    this.element.addEventListener('contextmenu', this.handleObjectEvent.bind(this, 'rightClick'));
  }

  handleObjectEvent(customEventName, event) {
    event.preventDefault();
    this.objects.forEach(obj => {
      const eventIndex = obj.events.findIndex(event => {
        return event.type === customEventName;
      });

      const xHitsArea = obj.hitAreas.x.map(pos => pos + obj.posX);
      const yHitsArea = obj.hitAreas.y.map(pos => pos + obj.posY);

      if (eventIndex > -1 && isPointInsidePolygon(xHitsArea.length, xHitsArea, yHitsArea, event.clientX, event.clientY)) {

        obj.events[eventIndex].list.forEach((_, fn)=> fn());
      }
    });
  }


  addListener(customEventName, callback) {
    this.element.addEventListener(customEventName, (event) => callback({x: event.clientX, y: event.clientY}));
  }

  delegate(object, actionCreator, actionParams) {
    const id = object.getId() + actionCreator;

    this.delegated.set(id, () => {
      return !object[actionCreator](...actionParams)();
    });
  }

  loop(callback) {
    const render = () => {
      this.context.clearRect(0, 0, this.element.width, this.element.height);

      this.delegated.forEach((fn, key) => {
        if (!fn()) {
          this.delegated.delete(key);
        }
      });

      callback.bind(this)();
      window.requestAnimationFrame(render);
    }

    window.requestAnimationFrame(render);
  }
};
