export function getUniqueId() {
  return '_' + Math.random().toString(36).substr(2, 9);
}

export function isObject(object) {
  return object === Object(object);
}
