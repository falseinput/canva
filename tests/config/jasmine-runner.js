import Jasmine from 'jasmine';

import canvaTests from '../canva-tests.js';
import geometryTests from '../geometry-tests.js';

const jasmine = new Jasmine();

canvaTests(jasmine.env);
geometryTests(jasmine.env);

jasmine.execute();
