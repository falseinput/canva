import { Canva } from '../src/canva.js';
import { CanvaError } from '../src/exceptions/canva-error.js';


export default function(jasmine) {
  jasmine.describe('Canva created with wrong param', function() {
    jasmine.it('Should throw Exception', function() {
      jasmine.expect(() => new Canva(2)).toThrow(new CanvaError('Canva error: Canva constructor requires object as argument.'));
    });
  });

  jasmine.describe('Canva created with missing required field', function() {
    const requiredFields = ['element'];

    requiredFields.forEach((field) => {
      jasmine.it('Should throw Exception', function() {
        jasmine.expect(() => new Canva({})).toThrow(new CanvaError(`Canva error: Param ${field} is required`));
      });
    });
  });
};
