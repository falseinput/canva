import { isPointInsidePolygon } from '../src/geometry.js';


export default function(jasmine) {
  jasmine.describe('isPointInsidePolygon', function() {
    const nvert = 4;
    const vertx = [-2, -2, 2, 2];
    const verty = [-2, 2, 2, -2];
    const testx = 0;
    const testy = 0;

    jasmine.it('should return true when point is inside polygon', function() {
      jasmine.expect(isPointInsidePolygon(nvert, vertx, verty, testx, testy)).toBe(true);
    });
  });

  jasmine.describe('isPointInsidePolygon', function() {
    const nvert = 4;
    const vertx = [-2, -2, 2, 2];
    const verty = [-2, 2, 2, -2];
    const testx = -2;
    const testy = 0;

    jasmine.it('should return true when point is on the polygon edge', function() {
      jasmine.expect(isPointInsidePolygon(nvert, vertx, verty, testx, testy)).toBe(true);
    });
  });
};
